![CI logo](https://codeinstitute.s3.amazonaws.com/fullstack/ci_logo_small.png)

Welcome,

This is the Code Institute student template for the mongo lessons. We have preinstalled all of the tools you need to get started. It's perfectly ok to use this template as the basis for your project submissions.

You can safely delete this README.md file, or change it for your own project. Please do read it at least once, though! It contains some important information about Codeanywhere and the extensions we use. Some of this information has been updated since the video content was created. The last update to this file was: **April 3rd, 2023**

## Codeanywhere Reminders

# IDE

- **Connect to Mongo CLI on a IDE**
- navigate to your MongoDB Clusters Sandbox
- click **"Connect"** button
- select **"Connect with the MongoDB shell"**
- select **"I have the mongo shell installed"**
- choose option 4.4 for : **"Select your mongo shell version"**
- choose option: **"Run your connection string in your command line"**
- `mongo "mongodb+srv://<CLUSTER-NAME>.mongodb.net/<DBname>" --username <USERNAME>`
  - replace all `<angle-bracket>` keys with your own data
- enter password *(will not echo **\*\*\*\*** *on screen)\*

#### Clear screen in Mongo Shell:

- `cls`

#### Show all database collections:

- `show collections`

To run a frontend (HTML, CSS, Javascript only) application in Codeanywhere, in the terminal, type:

`python3 -m http.server`

A button should appear to click: _Open Preview_ or _Open Browser_.

To run a backend Python file, type `python3 app.py`, if your Python file is named `app.py` of course.

A button should appear to click: _Open Preview_ or _Open Browser_.

In Codeanywhere you have superuser security privileges by default. Therefore you do not need to use the `sudo` (superuser do) command in the bash terminal in any of the lessons.

To log into the Heroku toolbelt CLI:

1. Log in to your Heroku account and go to _Account Settings_ in the menu under your avatar.
2. Scroll down to the _API Key_ and click _Reveal_
3. Copy the key
4. In Codeanywhere, from the terminal, run `heroku_config`
5. Paste in your API key when asked

You can now use the `heroku` CLI program - try running `heroku apps` to confirm it works. This API key is unique and private to you so do not share it. If you accidentally make it public then you can create a new one with _Regenerate API Key_.

---

Happy coding!

---

# Welcome to dependency hell.

![dependency_hell](/cartoons/dependencies.png)

The versions of the packages in the walkthrough source code no longer play nicely
together. To save you and futureMe some time, here's the versions that work together currently (last updated 13/1/2024).

Running the walkthrough code with the packages installed from the walkthrough requirements.txt file will give this error -

![jinja error](/doc_images/jinja_error.png)

So, after finding this SO post,
https://stackoverflow.com/questions/71718167/importerror-cannot-import-name-escape-from-jinja2
we install Flask==2.1.0.

Next, we get this error - 
![werkzeug error](/doc_images/werkzeug_error.png)

And, after finding this other SO post (https://stackoverflow.com/questions/77213053/why-did-flask-start-failing-with-importerror-cannot-import-name-url-quote-fr), we replace the recent version of werkzeug that Flask has as a dependency (because it apparently doesn't specify a version) with Werkzeug==2.3.7

This gives us this working set of versions - 

### requirements.txt  
`click==8.1.7`  
`dnspython==2.0.0`     
`Flask==2.1.0`  
`Flask-PyMongo==2.3.0`      
`itsdangerous==2.1.2`  
`pymongo==3.11.0`  
`Werkzeug==2.3.7`

# Connection String

With the above set of package versions, it was necessary to add the database name to the connection string provided by Mongo.

`mongodb+srv://johnrearden:z\<password>@cluster0.gi83zzo.mongodb.net/taskmanager?retryWrites=true&w=majority`

in order to avoid this error -

![attribute error](/doc_images/attribute_error.png)

## Python version
These versions work on gitpod with Python version 3.12.1

Between fresh installs of different package version combinations, I ran 
`pip3 freeze | xargs pip3 uninstall -y` to uninstall all installed packages.

